package sk.ite.castle.gateway.reservation.application.restapi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sk.ite.castle.gateway.reservation.application.dto.DTOReservation;
import sk.ite.castle.gateway.reservation.application.service.ReservationReader;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequestMapping("/reservations")
class ReservationApiGateway {
	private static final Logger LOG= LoggerFactory.getLogger(ReservationApiGateway.class);
	@Autowired
	private ReservationReader reservationReader;

	public List<DTOReservation> fallback(Long customerId) {
		return  Stream.of(new DTOReservation()).collect(Collectors.toList());
	}

	@RequestMapping(value = "/customer/{customerId}", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public List<DTOReservation> getAllReservationsForCustomer(@PathVariable("customerId") Long customerId) {
		LOG.info("Getting reservation fro customer {}.", customerId);
		return reservationReader.getAllReservationsForCustomer(customerId);
	}

}
