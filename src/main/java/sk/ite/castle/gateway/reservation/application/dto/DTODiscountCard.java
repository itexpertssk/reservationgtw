package sk.ite.castle.gateway.reservation.application.dto;

public class DTODiscountCard {
	public Long id;
	public String name;
	public Integer discountInPercentage;
}
