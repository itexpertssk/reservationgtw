package sk.ite.castle.gateway.reservation.application.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import sk.ite.castle.gateway.reservation.application.dto.DTOReservation;

import java.util.List;

@FeignClient("reservation")
public interface ReservationReader {

	@RequestMapping(method = RequestMethod.GET, value = "/reservations/customer/{customerId}")
	List<DTOReservation> getAllReservationsForCustomer(@PathVariable("customerId") Long customerId);

}



